/**
 * Mixin' the Colors.
 * A random walk on the grid with three primary colors.
 * 
 * Processing 3.5.3
 * @author @deconbatch
 * @version 0.1
 * created 0.1 2020.06.06
 */

void setup() {
  size(980, 640);
  colorMode(HSB, 360, 100, 100, 100);
  rectMode(CENTER);
  smooth();
  noLoop();
}

void draw() {

  int corners    = 3; // initial corners of the shape
  int patternMax = 3; // draw 3 patterns
  
  strokeJoin(ROUND);
  translate(width * 0.5, height * 0.5);

  for (int pattern = 0; pattern < patternMax; pattern++) {
    corners += pattern; // 3, 4, 6
    int walkerMax = 9;
    int stepMax   = corners * 200;
    float sideLen = 8.0 - pattern * 2.5;
    float fillAlp = (random(1.0) < 0.25) ? 0.0 : 100.0; // fill or not fill randomly
    strokeWeight(corners * corners * corners * random(0.01, 0.05));

    // set walker
    ArrayList<Walker> walkers = new ArrayList<Walker>();
    for (int i = 0; i < walkerMax; i++) {
      float wX = sideLen * corners * i - (sideLen * corners * walkerMax) * 0.5;
      float wY = 0.0;
      float wS = random(80.0, 100.0);
      float wB = random(0.0, 40.0);
      float wD = TWO_PI / corners;
      int   wR = floor(random(corners));
      walkers.add(new Walker(wX, wY,   0.0, wS, wB, wD, wR)); // cyan
      walkers.add(new Walker(wX, wY, 120.0, wS, wB, wD, wR)); // magenta
      walkers.add(new Walker(wX, wY, 240.0, wS, wB, wD, wR)); // yellow
    }

    // draw walker
    blendMode(BLEND);
    background(0.0, 0.0, 90.0, 100.0);
    blendMode(DIFFERENCE);
    for (Walker walker : walkers) {
      float wX = walker.x;
      float wY = walker.y;
      float wC = walker.hueVal;
      float wS = walker.satVal;
      float wB = walker.briVal;
      float wD = walker.radianDiv;
      float wR = walker.rotateCnt;
      stroke(wC, wS, wB, 60.0);
      fill(wC, wS, wB, fillAlp);

      // random walk
      for(int stepCnt = 0; stepCnt < stepMax; stepCnt++) {
        /*
         * Key of this code!
         * if (modCnt == 0) beginShape();
         * if (modCnt == 1) endShape();
         */
        int modCnt = stepCnt % (corners - 1);
        if (modCnt == 0) beginShape();
        for (int j = 0; j < corners; j++) {
          wX += sideLen * cos(wR * wD);
          wY += sideLen * sin(wR * wD);
          vertex(wX, wY);
        }
        if (modCnt == 1) endShape();
        
        if (abs(wX) < width * 0.35 && abs(wY) < height * 0.35) {
          if (random(1.0) < 0.5) {
            --wR; // not turn
          } else {
            if (random(1.0) < 0.1) {
              wD *= -1.0; // turn
            }
          }
        }
        
        ++wR;
        wR %= corners;
      }
    }
    blendMode(BLEND);
    casing();
    saveFrame("frames/" + String.format("%04d", pattern + 1) + ".png");
  }

  exit();

}

/**
 * casing : draw fancy casing
 */
private void casing() {
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(20.0);
  stroke(0.0, 0.0, 0.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(15.0);
  stroke(0.0, 0.0, 100.0, 100.0);
  rect(0.0, 0.0, width, height);
}

/**
 * Walker : hold random walker attributes
 */
private class Walker {
  public float x, y;
  public float hueVal, satVal, briVal;
  public float radianDiv;
  public int   rotateCnt;

  Walker(float _x, float _y, float _c, float _s, float _b, float _d, int _r) {
    x = _x;
    y = _y;
    hueVal = _c;
    satVal = _s;
    briVal = _b;
    radianDiv = _d;
    rotateCnt = _r;
  }
}
